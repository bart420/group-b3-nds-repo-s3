drop database if exists newspaper_delivery_system;
create database if not exists newspaper_delivery_system;
use newspaper_delivery_system;

drop table if exists customers;

CREATE TABLE customers (
    id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    last_name VARCHAR(30) NOT NULL,
    other_names VARCHAR(30) NOT NULL,
    date_of_birth VARCHAR(30) NOT NULL,
    phone_number VARCHAR(20) NOT NULL,
    address VARCHAR(150) NOT NULL,
    region VARCHAR(30) NOT NULL,
    delivery_frequency ENUM('daily', 'weekly', 'monthly') NOT NULL,
    subscription_id VARCHAR(50) NULL
);

ALTER TABLE customers 
ADD UNIQUE INDEX `phone_number_UNIQUE` (`phone_number` ASC),
ADD UNIQUE INDEX `address_UNIQUE` (`address` ASC);

drop table if exists assigned_regions_to_delivery_persons;
drop table if exists delivery_persons;
drop table if exists regions;

/*CREATE TABLE regions (
    id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    region_name VARCHAR(30) NOT NULL
);*/

CREATE TABLE delivery_persons (
    id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    username VARCHAR(30) NOT NULL,
    password VARCHAR(30) NOT NULL,
    phone_number VARCHAR(20) NOT NULL,
    address VARCHAR(150) NOT NULL,
    assigned_regions varchar(500) NOT NULL
);

/*CREATE TABLE assigned_regions_to_delivery_persons (
    id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    delivery_person_id integer not null,
    region_id integer not null
);

ALTER TABLE `assigned_regions_to_delivery_persons` 
ADD INDEX `fk_assigned_regions_to_delivery_persons_1_idx` (`delivery_person_id` ASC),
ADD INDEX `fk_assigned_regions_to_delivery_persons_2_idx` (`region_id` ASC);
ALTER TABLE `assigned_regions_to_delivery_persons` 
ADD CONSTRAINT `fk_assigned_regions_to_delivery_persons_1`
  FOREIGN KEY (`delivery_person_id`)
  REFERENCES `newspaper_delivery_system`.`delivery_persons` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_assigned_regions_to_delivery_persons_2`
  FOREIGN KEY (`region_id`)
  REFERENCES `newspaper_delivery_system`.`regions` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;*/

/*drop table if exists genres;

CREATE TABLE genres (
    id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    genre_name VARCHAR(30) NOT NULL
);*/

drop table if exists publications;

CREATE TABLE publications (
    id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    publication_name VARCHAR(50) NOT NULL,
    publication_date VARCHAR(30) NOT NULL,
    cost DECIMAL(6 , 2 ) NOT NULL,
    publication_frequency ENUM('daily', 'weekly', 'monthly') NOT NULL,
    genre VARCHAR(50) NOT NULL
);

/*drop procedure if exists create_delivery_person;
delimiter //
CREATE PROCEDURE create_delivery_person (
	in name_param varchar(50),
	in username_param varchar(20),
	in password_param varchar(20),
	in phoneNumber_param varchar(50),
	in address_param varchar(150),
	in assignedRegionIds_param varchar(150),
    out update_count integer
)
BEGIN
	declare sql_error tinyint default false;
    declare new_id integer;
    declare old_str varchar(150);
    declare an_id varchar(3);
	declare idx integer;
    declare str_len integer;
    
    declare continue handler for sqlexception
		set sql_error = true;

    start transaction;
    
    insert into delivery_persons (
		name,
        username,
        password,
        phone_number,
        address
    ) values (
		name_param,
        username_param,
        password_param,
        phoneNumber_param,
        address_param
    );
    
    set new_id = last_insert_id();
    set str_len = length(assignedRegionIds_param);
    
    while str_len > 0 do
		set old_str = assignedRegionIds_param;
        set idx = instr(old_str, ',');
        
        if idx = 0 then
			set an_id = trim(old_str);
            set assignedRegionIds_param = '';
        else
			set an_id = trim(substring(old_str, 1, idx-1));
            set assignedRegionIds_param = trim(substring(old_str, idx - str_len));
        end if;
        
		set str_len = length(assignedRegionIds_param);
        
		insert into assigned_regions_to_delivery_persons (
			delivery_person_id,
            region_id
        ) values (
			new_id,
            an_id
        );
    end while;
    
	if sql_error = false then
		set update_count = new_id; 
		commit;
	else
		set update_count = 0; 
		rollback;
    end if;
END //*/

drop table if exists newsagent_login;

CREATE TABLE newsagent_login (
    n_username VARCHAR(30) NOT NULL,
    n_password VARCHAR(30) NOT NULL
);

drop table if exists delivery_person_login;

CREATE TABLE delivery_person_login (
    d_username VARCHAR(30) NOT NULL,
    d_password VARCHAR(30) NOT NULL
);

insert into newsagent_login Values ("admin","password123");
