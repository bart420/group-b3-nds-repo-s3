
public class Publication {

	String publicationName;
	String publicationDate;
	double cost;
	String publicationFreq;
	String genere;

	String[] validPubName = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s",
			"t", "u", "y", "w", "v", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
			"P", "R", "S", "T", "U", "Y", "W", "V", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "+",
			"&", "�", "$", "!", " " };
	
	String[] validGenere = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s",
			"t", "u", "y", "w", "v", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
			"P", "R", "S", "T", "U", "Y", "W", "V", "Z", "-" };
	
	DatabaseConnector db;

	 public Publication(DatabaseConnector dbobj) {
		 db = dbobj;
	 }

	// Publication name Validation
	boolean validatePublicationName(String publicationName) {
		for (int i = 0; i < publicationName.length(); i++) {
			for (int j = 0; j < validPubName.length; j++) {
				if (publicationName.substring(i, i + 1).equals(validPubName[j])) {
					break;
				} else if ((j + 1) == validPubName.length) {
					return false;
				}
			}
		}
		return true;
	}

	// Publication date Validation
	boolean validatePublicationDate(String publicationDate) {
		boolean result = true;

		if (publicationDate.matches("\\d{2}-\\d{2}-\\d{2}")) {
			result = true;
		} else {
			result = false;
		}

		return result;

	}

	// Publication cost validation
	boolean validatePublicationCost(double cost) {
		boolean result = true;

		if (cost == 0 || cost < 0) {
			result = false;
		} else {
			result = true;
		}
		return result;

	}
	
	//Publication delivery frequency
	boolean validatePublicationFrequency(String publicationFreq) {
		boolean result = true;

		if (publicationFreq == "") {
			result = false;
		}

		return result;

	}
	
	// Publication genere Validation
		boolean validatePublicationGenere(String genere) {
			for (int i = 0; i < genere.length(); i++) {
				for (int j = 0; j < validGenere.length; j++) {
					if (genere.substring(i, i + 1).equals(validGenere[j])) {
						break;
					} else if ((j + 1) == validGenere.length) {
						return false;
					}
				}
			}
			return true;
		}
		
		
		public String createPublication(String publicationName, String publicationDate, double cost, String publicationFreq, String genere) throws Exception {
			String result = "ok";
			boolean vr1 = validatePublicationName(publicationName);
			boolean vr2 = validatePublicationDate(publicationDate);
			boolean vr3 = validatePublicationCost(cost);
			boolean vr4 = validatePublicationFrequency(publicationFreq);
			boolean vr5 = validatePublicationGenere(genere);

			if (vr1 == false) {
				result = "Incorrect Publication Name";
			} else if (vr2 == false) {
				result = "Incorrect Publication Date";
			} else if (vr3 == false) {
				result = "Incorrect Publication Cost";
			} else if (vr4 == false) {
				result = "Incorrect Publication Frequency";
			} else if (vr5 == false) {
				result = "Incorrect Publication Genere";
			} else if ((vr1 == true) && (vr2 == true) && (vr3 == true) && (vr4 == true) && (vr5 == true)) {
				result = db.createPublicationInDatabase(publicationName, publicationDate, cost, publicationFreq, genere) ? "ok" : "fail";
			}
			return result;
		}

}
