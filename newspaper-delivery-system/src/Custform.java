import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;
import javax.swing.ButtonGroup;

public class Custform extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField_4;
	private Customer customer;
	String freq;
	private JFrame myFrame;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Custform frame = new Custform();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/
	public static void NewScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Custform frame = new Custform(dbobj);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Custform(DatabaseConnector dbobj) {
		super("Customer Form");
		myFrame = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 530, 448);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		customer = new Customer(dbobj);
		
		JLabel lblNewLabel = new JLabel("Customer Details");
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 16));
		lblNewLabel.setBounds(164, 2, 244, 49);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Last Name:");
		lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(37, 64, 81, 15);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblOtherName = new JLabel("Other Name:");
		lblOtherName.setFont(new Font("宋体", Font.PLAIN, 13));
		lblOtherName.setBounds(37, 89, 81, 15);
		contentPane.add(lblOtherName);
		
		JLabel lblDob = new JLabel("DOB:");
		lblDob.setFont(new Font("宋体", Font.PLAIN, 13));
		lblDob.setBounds(37, 120, 81, 15);
		contentPane.add(lblDob);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("宋体", Font.PLAIN, 13));
		lblAddress.setBounds(37, 183, 81, 15);
		contentPane.add(lblAddress);
		
		JLabel lblRegion = new JLabel("Region:");
		lblRegion.setFont(new Font("宋体", Font.PLAIN, 13));
		lblRegion.setBounds(37, 256, 81, 15);
		contentPane.add(lblRegion);
		
		JLabel lblDeliveryFrequency = new JLabel("Delivery Frequency:");
		lblDeliveryFrequency.setFont(new Font("宋体", Font.PLAIN, 13));
		lblDeliveryFrequency.setBounds(37, 292, 133, 15);
		contentPane.add(lblDeliveryFrequency);
		

		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(287, 61, 150, 97);
		contentPane.add(lblNewLabel_2);
		
		textField = new JTextField();
		textField.setBounds(128, 61, 96, 21);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(128, 86, 96, 21);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(128, 117, 96, 21);
		contentPane.add(textField_2);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Daily");
		buttonGroup.add(rdbtnNewRadioButton);
		//rdbtnNewRadioButton.setKeyAccelerator('1');
		//rdbtnNewRadioButton.setActionCommand("Daily");
		//rdbtnNewRadioButton.setSelected(true);
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				freq = "daily";
			}
		});
		rdbtnNewRadioButton.setBounds(200, 288, 69, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Weekly");
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				freq = "weekly";
			}
		});
		//rdbtnNewRadioButton.setActionCommand("Daily");
		//rdbtnNewRadioButton.setSelected(true);
		rdbtnNewRadioButton_1.setBounds(287, 288, 121, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Monthly");
		buttonGroup.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				freq = "monthly";
			}
		});
		rdbtnNewRadioButton_2.setBounds(201, 315, 121, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(128, 179, 115, 67);
		contentPane.add(textArea_1);
		
		JLabel lblPhoneNumber = new JLabel("Phone Number:");
		lblPhoneNumber.setFont(new Font("宋体", Font.PLAIN, 13));
		lblPhoneNumber.setBounds(33, 154, 96, 15);
		contentPane.add(lblPhoneNumber);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(128, 148, 96, 21);
		contentPane.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(128, 253, 96, 21);
		contentPane.add(textField_4);
		
		JButton btnNewButton = new JButton("Create");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				String r = customer.createCustomer(
						textField.getText(),
						textField_1.getText(),
						textField_2.getText(),
						textArea_1.getText(),
						textField_4.getText(),
						freq,
						textField_3.getText()
				);
				JOptionPane.showMessageDialog(null, r, "Information", JOptionPane.INFORMATION_MESSAGE);
		
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			}
		});
		btnNewButton.setFont(new Font("宋体", Font.PLAIN, 13));
		btnNewButton.setBounds(37, 355, 115, 32);
		contentPane.add(btnNewButton);
		
		
		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				CustChoose nw=new CustChoose(dbobj);
				nw.NewScreen4(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		btnCancel.setFont(new Font("宋体", Font.PLAIN, 13));
		btnCancel.setBounds(229, 355, 115, 32);
		contentPane.add(btnCancel);
		
		
	}
}
