import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class DeliveryPersonLogin {

	private JFrame frame;
	private JTextField textField;
	private JPasswordField passwordField;
	private DatabaseConnector dbc;

	/**
	 * Launch the application.
	 */
	public static void DeliveryPersonLoginScreen(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeliveryPersonLogin window = new DeliveryPersonLogin(dbobj);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @param dbc 
	 * @throws Exception 
	 */
	public DeliveryPersonLogin(DatabaseConnector dbc) throws Exception {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void initialize() throws Exception {
		dbc = new DatabaseConnector();
		frame = new JFrame("Delivery Person Login");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblLogin.setBounds(186, 11, 75, 23);
		frame.getContentPane().add(lblLogin);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblUsername.setBounds(53, 79, 81, 23);
		frame.getContentPane().add(lblUsername);
		
		textField = new JTextField();
		textField.setBounds(186, 82, 182, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPassword.setBounds(53, 126, 81, 23);
		frame.getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setToolTipText("");
		passwordField.setBounds(186, 129, 182, 20);
		frame.getContentPane().add(passwordField);
		
		Button button = new Button("<< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					StartPage nw=new StartPage(dbc);
					nw.StartScreen(dbc);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				
				try {
					frame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button.setBounds(76, 206, 70, 22);
		frame.getContentPane().add(button);
		
		Button button_1 = new Button("Login");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					boolean r = dbc.checkDeliveryDriverLogin(textField.getText(),Arrays.toString(passwordField.getPassword()).replace("[",  "").replace("]", "").replace(", ", ""));
					if (r) {
					DeliveryPersonMenu nw = new DeliveryPersonMenu(dbc);
					nw.DeliveryPersonMenuScreen(dbc, textField.getText());
					frame.setVisible(false);
					} else {
						JOptionPane.showMessageDialog(frame, "Login attempt unsuccessful. Please, try again", "Information", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		button_1.setBounds(283, 206, 70, 22);
		frame.getContentPane().add(button_1);
	}
}
