import java.sql.ResultSet;
import java.util.Arrays;

public class Customer {

	String[] invalidChars = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "!", "'", "�", "$", "%", "^", "&", "*",
			"(", ")", "-", "_", "+", "=", "�", "`", "[", "]", "{", "}", "#", "@", "\\", "/", "|", "<", ">", ",", ".",
			"~", "\"", "?" };

	String[] invalidCharsAddress = { "!", "'", "�", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "�", "`",
			"[", "]", "{", "}", "#", "@", "\\", "|", "<", ">", "~", "\"", "?" };

	String[] validNumber = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

	int id;
	String lastName;
	String otherNames;
	String dateOfBirth;
	String address;
	String region;
	String deliveryFrequency;
	String phoneNumber;
	String customerPublications;
	DatabaseConnector db;


	 public Customer(DatabaseConnector dbobj) {
		 db = dbobj;
	 }
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
		
	public void setLastName(String ls) {
		this.lastName = ls;
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public void setOtherNames(String os) {
		this.otherNames = os;
	}
	
	public String getOtherNames() {
		return this.otherNames;
	}
	
	public void setDateOfBirth(String dob) {
		this.dateOfBirth = dob;
	}
	
	public String getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setAddress(String addy) {
		this.address = addy;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setRegion(String reg) {
		this.region = reg;
	}
	
	public String getRegion() {
		return this.region;
	}
	
	public void setDeliveryFrequency(String delF) {
		this.deliveryFrequency = delF;
	}
	
	public String getDeliveryFrequency() {
		return this.deliveryFrequency;
	}
	
	public void setPhoneNumber(String phone) {
		this.phoneNumber = phone;
	}
	
	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	public void setCustomerPublications(String aStr) {
		this.customerPublications = aStr;
	}
	
	public String getCustomerPublications() {
		return this.customerPublications;
	}

	boolean validateLastName(String lastName) {
		boolean result = true;

		for (int i = 0; i < invalidChars.length; i++) {
			if (lastName.indexOf(invalidChars[i]) >= 0) {
				result = false;
				break;
			}
		}

		return result;
	}

	boolean validateOtherNames(String otherNames) {
		boolean result = true;

		for (int i = 0; i < invalidChars.length; i++) {
			if (otherNames.indexOf(invalidChars[i]) >= 0) {
				result = false;
				break;
			}
		}

		return result;

	}

	boolean validateDOB(String dateOfBirth) {
		boolean result = true;

		if (dateOfBirth.matches("\\d{2}-\\d{2}-\\d{2}")) {
			result = true;
		} else {
			result = false;
		}

		return result;

	}

	boolean validateAddress(String address) {
		boolean result = true;

		for (int i = 0; i < invalidCharsAddress.length; i++) {
			if (address.indexOf(invalidCharsAddress[i]) >= 0) {
				result = false;
				break;
			}
		}

		return result;

	}

	boolean validateRegion(String region) {
		boolean result = true;

		if (region == "") {
			result = false;
		}

		return result;

	}

	boolean validateDeliveryFrequency(String deliveryFrequency) {
		boolean result = true;

		if (deliveryFrequency == "0") {
			result = false;
		}

		return result;

	}

	boolean validatePhoneNumber(String phoneNumber) {
		for (int i = 0; i < phoneNumber.length(); i++) {
			for (int j = 0; j < validNumber.length; j++) {
				if (phoneNumber.substring(i, i + 1).equals(validNumber[j])) {
					break;
				} else if ((j + 1) == validNumber.length) {
					return false;
				}
			}
		}
		
		return true;
	}

	public String createCustomer(String lastName, String otherNames, String dateOfBirth, String address,String region,
			String deliveryFrequency, String phoneNumber) throws Exception{
		String result = "ok";
		boolean vr1 = validateLastName(lastName);
		boolean vr2 = validateOtherNames(otherNames);
		boolean vr3 = validateDOB(dateOfBirth);
		boolean vr4 = validateAddress(address);
		boolean vr5 = validateRegion(region);
		boolean vr6 = validateDeliveryFrequency(deliveryFrequency);
		boolean vr7 = validatePhoneNumber(phoneNumber);

		if (vr1 == false) {
			result = "Incorrect Last Name";
		} else if (vr2 == false) {
			result = "Incorrect Other Name";
		} else if (vr3 == false) {
			result = "Incorrect DOB";
		} else if (vr4 == false) {
			result = "Incorrect Address";
		} else if (vr5 == false) {
			result = "Incorrect Region";
		} else if (vr6 == false) {
			result = "Incorrect Delivery Freq";
		} else if (vr7 == false) {
			result = "Incorrect Phone Number";
		} else if ((vr1 == true) && (vr2 == true) && (vr3 == true) && (vr4 == true) && (vr5 == true) && (vr6 == true)
				&& (vr7 == true)) {
			result = db.createCustomerInDatabase(lastName, otherNames, dateOfBirth, phoneNumber, address, region,
					deliveryFrequency) ? "ok" : "fail";
		}
		return result;
	}
	
	boolean validateCustomerId(int customerId) throws Exception {
		if (customerId > 0 && customerId <= Integer.MAX_VALUE)
			return true
		; else
			return false
		;
	}
	
	boolean validatePublicationIds(int[] publicationIds) throws Exception {
		int i = 0;
		
		while (i < publicationIds.length) {
			if (publicationIds[i] <= 0 || publicationIds[i] > Integer.MAX_VALUE)
				return false
			;
			
			i++;
		}
		
		return true;
	}

	public String editCustomerSubscriptions(int customerId, int[] publicationIds) throws Exception {
		boolean r = validateCustomerId(customerId);
		boolean r1 = validatePublicationIds(publicationIds);
		
		if (r == false && r1 == false)
			return "Select a customer and the required publication(s)."
		; else if (r == false) 
			return "Select a customer."
		; else if (r1 == false)
			return "Select the required publication(s)."
		; else {
			return db.updateCustomerSubscriptions(Arrays.toString(publicationIds).replace("[", "").replace("]", ""), customerId)
					? "Customer has been successfully subscribed to the selected publication(s)."
					:"An error occurred while trying to add publications to the customer's subscription."
			;
		}
	}
	
	public ResultSet viewCustomerDetails(String customerName) throws Exception {
		if (!customerName.trim().equals("")) {
			ResultSet rs = db.retrieveCustomerDetails(customerName);
			//Customer[] customerArr;
			//int i = 0;
			
//			rs.last();
//			customerArr = new Customer[rs.getRow()];
//			rs.beforeFirst();
			
//			while (rs.next()) {
//				Customer c = new Customer(db);
//				
//				c.setId(rs.getInt("id"));
//				c.setLastName(rs.getString("last_name"));
//				c.setOtherNames(rs.getString("other_names"));
//				c.setDateOfBirth(rs.getString("date_of_birth"));
//				c.setPhoneNumber(rs.getString("phone_number"));
//				c.setAddress(rs.getString("address"));
//				c.setRegion(rs.getString("region"));
//				c.setDeliveryFrequency(rs.getString("delivery_frequency"));
//				c.setCustomerPublications(db.convertSubscriptions(rs.getString("subscription_id")));
//				customerArr[i] = c;
//			}
			
			return rs; 
		} else
			return null
		;
	}
}


