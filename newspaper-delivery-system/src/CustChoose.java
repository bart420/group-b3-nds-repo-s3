import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CustChoose extends JFrame {

	private JPanel contentPane;
	private JFrame myFrame;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustChoose frame = new CustChoose();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}*/
	public static void NewScreen4(DatabaseConnector dbobj) {
	EventQueue.invokeLater(new Runnable() {
		public void run() {
			try {
				CustChoose frame = new CustChoose(dbobj);
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	});
	}

	/**
	 * Create the frame.
	 */
	public CustChoose(DatabaseConnector dbobj) {
		super("Customer Choose");
		myFrame = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 378);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Add New Customer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				Custform nw=new Custform(dbobj);
				nw.NewScreen(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("����", Font.PLAIN, 16));
		btnNewButton.setBounds(119, 11, 191, 67);
		contentPane.add(btnNewButton);
		
		JButton btnSearchCustomer = new JButton("Search Customer");
		btnSearchCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ViewCustomer nw=new ViewCustomer(dbobj);
				nw.ViewCustomersScreen(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		btnSearchCustomer.setFont(new Font("����", Font.PLAIN, 16));
		btnSearchCustomer.setBounds(119, 89, 191, 67);
		contentPane.add(btnSearchCustomer);
		
		JButton btnNewButton_1 = new JButton("BACK");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				choosepage nw=new choosepage(dbobj);
				nw.NewScreen3(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
			
		});
		btnNewButton_1.setFont(new Font("����", Font.PLAIN, 16));
		btnNewButton_1.setBounds(157, 270, 106, 36);
		contentPane.add(btnNewButton_1);
		
		JButton btnAddSubscriptions = new JButton("Add Subscriptions");
		btnAddSubscriptions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					CustomerSubscriptions nw=new CustomerSubscriptions(dbobj);
					nw.AddSubScreen(dbobj);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
				
			}
		});
		btnAddSubscriptions.setFont(new Font("Dialog", Font.PLAIN, 16));
		btnAddSubscriptions.setBounds(119, 167, 191, 67);
		contentPane.add(btnAddSubscriptions);
	}
}
