import java.sql.ResultSet;
import java.util.Arrays;
import junit.framework.TestCase;

public class DatabaseConnectorTest extends TestCase {	
	// Test Number: 1
	// Test Objective: All valid parameters are valid
	// Inputs: lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", phoneNumber = "0899999991", address = "Number 55 Dublin Road Athlone Westmeath", region = "ATH 1", deliveryFrequency = "daily"
	// Expected Output: True
	public void testcreateCustomerInDatabse001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(true, dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0899999991", 
					"Number 55 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 2
	// Test Objective: All invalid parameters are not valid
	// Inputs: lastName = "", otherNames ="", dateOfBirth = "", phoneNumber = "", address = "", region = "", deliveryFrequency = ""
	// Expected Output: False
	public void testcreateCustomerInDatabse002() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			boolean result = dbc.createCustomerInDatabase("", "", "", "", "", "", "");
			
			assertEquals(false, result);
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}

	// Test Number: 3
	// Test Objective: Only lastName is not valid
	// Inputs: lastName = "", otherNames = "Other Names", dateOfBirth = "1980-01-01", phoneNumber = "0899999919", address = "Number 56 Dublin Road Athlone Westmeath", region = "ATH 1", deliveryFrequency = "daily"
	// Expected Output: False
	public void testcreateCustomerInDatabse003() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createCustomerInDatabase(
					"", 
					"Other Names", 
					"1980-01-01", 
					"0899999999", 
					"Number 55 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 4
	// Test Objective: Only otherNames is not valid
	// Inputs: lastName = "LastName", otherNames = "", dateOfBirth = "1980-01-01", phoneNumber = "0899999199", address = "Number 57 Dublin Road Athlone Westmeath", region = "ATH 1", deliveryFrequency = "daily"
	// Expected Output: False
	public void testcreateCustomerInDatabse004() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createCustomerInDatabase(
					"LastName", 
					"", 
					"1980-01-01", 
					"0899999999", 
					"Number 55 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 5
	// Test Objective: Only dateOfBirth is not valid
	// Inputs: lastName = "LastName", otherNames = "Other Names", dateOfBirth = "", phoneNumber = "0899991999", address = "Number 58 Dublin Road Athlone Westmeath", region = "ATH 1", deliveryFrequency = "daily"
	// Expected Output: False
	public void testcreateCustomerInDatabse005() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"", 
					"0899999999", 
					"Number 55 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 6
	// Test Objective: Only phoneNumber is not valid
	// Inputs: lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", phoneNumber = "", address = "Number 59 Dublin Road Athlone Westmeath", region = "ATH 1", deliveryFrequency = "daily"
	// Expected Output: False
	public void testcreateCustomerInDatabse006() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"", 
					"Number 55 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 7
	// Test Objective: Only address is not valid
	// Inputs: lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", phoneNumber = "0899919999", address = "", region = "ATH 1", deliveryFrequency = "daily"
	// Expected Output: False
	public void testcreateCustomerInDatabse007() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0899999999", 
					"", 
					"ATH 1", 
					"daily"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 8
	// Test Objective: Only region is not valid
	// Inputs: lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", phoneNumber = "0899199999", address = "Number 60 Dublin Road Athlone Westmeath", region = "", deliveryFrequency = "daily"
	// Expected Output: False
	public void testcreateCustomerInDatabse008() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0899999999", 
					"Number 55 Dublin Road Athlone Westmeath", 
					"", 
					"daily"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 9
	// Test Objective: Only deliveryFrequency is not valid
	// Inputs: lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", phoneNumber = "0891999999", address = "Number 61 Dublin Road Athlone Westmeath", region = "ATH 1", deliveryFrequency = ""
	// Expected Output: False
	public void testcreateCustomerInDatabse009() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0899999999", 
					"Number 55 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					""
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 10
	// Test Objective: Addresses should not be the same
	// Inputs: Set 1 - lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", 
	//   phoneNumber = "0819999999", address = "Number 62 Dublin Road Athlone Westmeath", 
	//   region = "ATH 1", deliveryFrequency = "daily"
	//   Set 2 - lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", 
	//   phoneNumber = "0809999999", address = "Number 62 Dublin Road Athlone Westmeath", 
	//   region = "ATH 1", deliveryFrequency = "daily"
	// Expected Output: Exception.
	public void testcreateCustomerInDatabse010() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			boolean result = true; // assuming default true
			boolean result1 = true; // assuming default true
			
			result = dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0819999999", 
					"Number 62 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			);
			
			// should return false because of exact matching address
			result1 = dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0809999999", 
					"Number 62 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			);

			fail("Exception should be thrown.");
		} catch(Exception e) {
			assertTrue(true);
		}
	}
	
	// Test Number: 11
	// Test Objective: Phone numbers should not be the same
	// Inputs: Set 1 - lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", 
	//   phoneNumber = "0819999999", address = "Number 63 Dublin Road Athlone Westmeath", 
	//   region = "ATH 1", deliveryFrequency = "daily"
	//   Set 2 - lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", 
	//   phoneNumber = "0809999999", address = "Number 64 Dublin Road Athlone Westmeath", 
	//   region = "ATH 1", deliveryFrequency = "daily"
	// Expected Output: Exception
	public void testcreateCustomerInDatabse011() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			boolean result = true; // assuming default true
			boolean result1 = true; // assuming default true
			
			result = dbc.createCustomerInDatabase(
					"LastName", 
					"James", 
					"1980-01-01", 
					"0809999991", 
					"Number 63 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			);
			
			// should return false because of exact matching phoneNumber
			result1 = dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0809999991", 
					"Number 64 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"daily"
			);
			
			fail("Exception should be thrown.");
		} catch(Exception e) {
			assertTrue(true);
		}
	}
	
	// Test Number: 12
	// Test Objective: Valid deliveryFrequency values (daily, weekly, monthly) should be accepted
	// Inputs: Set 1 - lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", 
	//   phoneNumber = "0819999999", address = "Number 62 Dublin Road Athlone Westmeath", 
	//   region = "ATH 1", deliveryFrequency = "weekly"
	//   Set 2 - lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", 
	//   phoneNumber = "0809999999", address = "Number 62 Dublin Road Athlone Westmeath", 
	//   region = "ATH 1", deliveryFrequency = "monthly"
	// Expected Output: True
	public void testcreateCustomerInDatabse012() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			boolean result1 = false; // assuming default false
			boolean result2 = false; // assuming default false
			
			result1 = dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0809999993", 
					"Number 66 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"weekly"
			);
			
			result2 = dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0809999994", 
					"Number 67 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"monthly"
			);
			
			assertEquals(true, result1 && result2);
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	//Test Number: 13
	// Test Objective: Values asides (daily, weekly, monthly) should not be accepted
	// Inputs: lastName = "LastName", otherNames = "Other Names", dateOfBirth = "1980-01-01", 
	//   phoneNumber = "0809999999", address = "Number 62 Dublin Road Athlone Westmeath", 
	//   region = "ATH 1", deliveryFrequency = "bi-annually"
	// Expected Output: False
	public void testcreateCustomerInDatabse013() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createCustomerInDatabase(
					"LastName", 
					"Other Names", 
					"1980-01-01", 
					"0809999992", 
					"Number 65 Dublin Road Athlone Westmeath", 
					"ATH 1", 
					"bi-annually"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}

	// Test Number: 13
	// Test Objective: A name must be supplied for creating regions
	// Inputs: regionName = "Region 1"
	// Expected Output: True
//	public void testcreateRegionInDatabse013() {
//		try {
//			DatabaseConnector dbc = new DatabaseConnector();
//
//			assertEquals(true, dbc.createRegionInDatabase("Region 1"));
//			dbc.createRegionInDatabase("Region 2"); // regionid 2
//			dbc.createRegionInDatabase("Region 3"); // regionid 3
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	// Test Number: 14
	// Test Objective: A region cannot be created without a name
	// Inputs: regionName = ""
	// Expected Output: True
//	public void testcreateCustomerInDatabse014() {
//		try {
//			DatabaseConnector dbc = new DatabaseConnector();
//
//			assertEquals(false, dbc.createRegionInDatabase(""));
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	// Test Number: 15
	// Test Objective: All parameter are valid
	// Inputs: name = "A Delivery Person", username = "dp1", password = "dp123", 
	// phoneNumber = "0812345678", address = "No. 1 Delivery Person Street Athlone", assignedRegions = "ATH 1, ATH 2, ATH 3"
	// Expected Output: True
	public void testcreateDeliveryPersonInDatabse015() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(true, dbc.createDeliveryPersonInDatabase(
					"A Delivery Person", 
					"dp1", 
					"dp123", 
					"0812345678", 
					"No. 1 Delivery Person Street Athlone",
					"ATH 1, ATH 2, ATH 3"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 16
	// Test Objective: All parameter are not valid
	// Inputs: name = "", username = "", password = "", phoneNumber = "", address = "", assignedRegions = ""
	// Expected Output: False
	public void testcreateDeliveryPersonInDatabse016() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createDeliveryPersonInDatabase(
					"", 
					"", 
					"", 
					"", 
					"",
					""
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}

	// Test Number: 17
	// Test Objective: Only name is not valid
	// Inputs: name = "", username = "dp1", password = "dp123", phoneNumber = "0812345678",
	// address = "No. 1 Delivery Person Street Athlone", assignedRegions = "ATH 1, ATH 2, ATH 3"
	// Expected Output: False
	public void testcreateDeliveryPersonInDatabse017() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createDeliveryPersonInDatabase(
					"", 
					"dp1", 
					"dp123", 
					"0812345678", 
					"No. 1 Delivery Person Street Athlone",
					"ATH 1, ATH 2, ATH 3"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 18
	// Test Objective: Only username is not valid
	// Inputs: name = "A Delivery Person", username = "", password = "dp123", phoneNumber = "0812345678", 
	// address = "No. 1 Delivery Person Street Athlone", assignedRegions = "ATH 1, ATH 2, ATH 3"
	// Expected Output: False
	public void testcreateDeliveryPersonInDatabse018() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createDeliveryPersonInDatabase(
					"A Delivery Person", 
					"", 
					"dp123", 
					"0812345678", 
					"No. 1 Delivery Person Street Athlone",
					"ATH 1, ATH 2, ATH 3"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 19
	// Test Objective: Only password is not valid
	// Inputs: name = "A Delivery Person", username = "dp1", password = "", phoneNumber = "0812345678", 
	// address = "No. 1 Delivery Person Street Athlone", assignedRegions = "ATH 1, ATH 2, ATH 3"
	// Expected Output: False
	public void testcreateDeliveryPersonInDatabse019() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createDeliveryPersonInDatabase(
					"A Delivery Person", 
					"dp1", 
					"", 
					"0812345678", 
					"No. 1 Delivery Person Street Athlone",
					"ATH 1, ATH 2, ATH 3"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 20
	// Test Objective: Only phoneNumber is not valid
	// Inputs: name = "A Delivery Person", username = "dp1", password = "dp123", phoneNumber = "", 
	// address = "No. 1 Delivery Person Street Athlone", assignedRegions = "ATH 1, ATH 2, ATH 3"
	// Expected Output: False
	public void testcreateDeliveryPersonInDatabse020() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createDeliveryPersonInDatabase(
					"A Delivery Person", 
					"dp1", 
					"dp123", 
					"", 
					"No. 1 Delivery Person Street Athlone",
					"ATH 1, ATH 2, ATH 3"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 21
	// Test Objective: Only address is not valid
	// Inputs: name = "A Delivery Person", username = "dp1", password = "dp123", phoneNumber = "0812345678", 
	// address = "", assignedRegions = "ATH 1, ATH 2, ATH 3"
	// Expected Output: False
	public void testcreateDeliveryPersonInDatabse021() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			
			assertEquals(false, dbc.createDeliveryPersonInDatabase(
					"A Delivery Person", 
					"dp1", 
					"dp123", 
					"0812345678", 
					"",
					"ATH 1, ATH 2, ATH 3"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 22
	// Test Objective: Only address is not valid
	// Inputs: name = "A Delivery Person", username = "dp1", password = "dp123", phoneNumber = "0812345678", 
	// address = "No. 1 Delivery Person Street Athlone", assignedRegions = ""
	// Expected Output: False
	public void testcreateDeliveryPersonInDatabse022() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createDeliveryPersonInDatabase(
					"A Delivery Person", 
					"dp1", 
					"dp123", 
					"0812345678", 
					"No. 1 Delivery Person Street Athlone",
					""
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 25
	// Test Objective: All parameters are valid
	// Inputs: publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 3.99, publicationFrequency = "daily", genre = "News"
	// Expected Output: True
	public void testcreatePublicationInDatabase025() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"2017-02-19", 
					3.99, 
					"daily", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 26
	// Test Objective: All parameters are not valid
	// Inputs: publicationName = "", publicationDate = "", cost = 0, publicationFrequency = "", genre = ""
	// Expected Output: False
	public void testcreatePublicationInDatabase026() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"", 
					"", 
					0, 
					"", 
					""
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 27
	// Test Objective: Only publicationName is not valid
	// Inputs: publicationName = "", publicationDate = "2017-02-19", cost = 3.99, publicationFrequency = "daily", genre = "News"
	// Expected Output: False
	public void testcreatePublicationInDatabase027() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"", 
					"2017-02-19", 
					3.99, 
					"daily", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 28
	// Test Objective: Only publicationDate is not valid
	// Inputs: publicationName = "Athlone Daily", publicationDate = "", cost = 3.99, publicationFrequency = "daily", genre = "News"
	// Expected Output: False
	public void testcreatePublicationInDatabase028() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"", 
					3.99, 
					"daily", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 29
	// Test Objective: Only cost is equal to zero
	// Inputs: publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 0, publicationFrequency = "daily", genre = "News"
	// Expected Output: False
	public void testcreatePublicationInDatabase029() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"2017-02-19", 
					0, 
					"daily", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	//Test Number: 30
	// Test Objective: Only cost is less than zero
	// Inputs: publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = -1, publicationFrequency = "daily", genre = "News"
	// Expected Output: False
	public void testcreatePublicationInDatabase030() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"2017-02-19", 
					-1, 
					"daily", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 31
	// Test Objective: Only cost is greater than Double.MAX_VALUE
	// Inputs: publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 1001, publicationFrequency = "daily", genre = "News"
	// Expected Output: False
	public void testcreatePublicationInDatabase031() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"2017-02-19", 
					1001, 
					"daily", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	//Test Number: 32
	// Test Objective: All are valid and cost is equal to 1000
	// Inputs: publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 1000, publicationFrequency = "daily", genre = "News"
	// Expected Output: True
	public void testcreatePublicationInDatabase032() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"2017-02-19", 
					1000, 
					"daily", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 33
	// Test Objective: Only publicationFrequency is not daily, weekly or monthly
	// Inputs: publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 3.99, publicationFrequency = "bi-annually", genre = "News"
	// Expected Output: False
	public void testcreatePublicationInDatabase033() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"2017-02-19", 
					3.99, 
					"bi-annually", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 34
	// Test Objective: Only publicationFrequency is an empty string and not valid
	// Inputs: publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 3.99, publicationFrequency = "", genre = "News"
	// Expected Output: False
	public void testcreatePublicationInDatabase034() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"2017-02-19", 
					3.99, 
					"", 
					"News"
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 35
	// Test Objective: All are valid and publicationFrequency is weekly or monthly
	// Inputs: Set 1 - publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 3.99, publicationFrequency = "weekly", genre = "News"
	// Set 2 - publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 3.99, publicationFrequency = "monthly", genre = "News"
	// Expected Output: True
	public void testcreatePublicationInDatabase035() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();
			boolean result1 = false; // assume wrong
			boolean result2 = false; // assume wrong

			result1 = dbc.createPublicationInDatabase(
					"Mullingar Times", 
					"2017-02-19", 
					3.99, 
					"weekly", 
					"News"
			);
			
			result2 = dbc.createPublicationInDatabase(
					"Westmeath Independant", 
					"2017-02-19", 
					3.99, 
					"monthly", 
					"News"
			);
			
			assertEquals(true, result1 && result2);
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	// Test Number: 36
	// Test Objective: Only genre is not valid
	// Inputs: publicationName = "Athlone Daily", publicationDate = "2017-02-19", cost = 3.99, publicationFrequency = "daily", genre = ""
	// Expected Output: False
	public void testcreatePublicationInDatabase036() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(false, dbc.createPublicationInDatabase(
					"Athlone Daily", 
					"2017-02-19", 
					3.99, 
					"daily", 
					""
			));
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void testupdateCustomerSubscriptions001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.updateCustomerSubscriptions("1,2",1));
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	
	public void testretrieveCustomerName001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.retrieveCustomerName() );
			
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	
	public void testretrieveCustomerSubscriptions001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.retrieveCustomerSubscriptions(1)instanceof String);
			
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	public void testretrieveCustomerSubscriptions002() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.retrieveCustomerSubscriptions(2)instanceof String);
			
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	public void testcheckNewsAgentsLogin001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.checkNewsAgentsLogin("admin","password123"));
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	
	public void testcheckDeliveryDriverLogin001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.checkDeliveryDriverLogin("dp1","dp123"));
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	
	public void testretrieveDeliveryDriverDetails001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.retrieveDeliveryDriverDetails("") instanceof ResultSet);
			
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}

	
	public void testretrieveCustomerDetails001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.retrieveCustomerDetails("oth") );
			
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	
	public void testconvertSubscription001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.convertSubscriptions("1,3,4") instanceof String);
			
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	public void testretrievePublicationName001() {
		try {
			DatabaseConnector dbc = new DatabaseConnector();

			assertEquals(true, dbc.retrievePublicationName() );
			
			
		} catch(Exception e) {
			e.printStackTrace();
			fail("Method not implemented yet");
		}
	}
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
}