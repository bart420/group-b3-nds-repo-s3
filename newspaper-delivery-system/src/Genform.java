import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Genform extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Genform frame = new Genform();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public static void NewScreen12() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Genform frame = new Genform();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Genform() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGenreForm = new JLabel("Genre Form");
		lblGenreForm.setFont(new Font("Dialog", Font.PLAIN, 16));
		lblGenreForm.setBounds(151, 11, 244, 49);
		contentPane.add(lblGenreForm);
		
		JLabel lblGenre = new JLabel("Genre:");
		lblGenre.setBounds(29, 69, 46, 14);
		contentPane.add(lblGenre);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(118, 66, 124, 20);
		contentPane.add(textField);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(29, 109, 89, 14);
		contentPane.add(lblName);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(118, 106, 86, 20);
		contentPane.add(textField_1);
		
		JButton button = new JButton("Add");
		button.setBounds(101, 209, 89, 23);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Cancel");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				choosepage nw=new choosepage(null);
				nw.NewScreen3(null);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			}
		});
		button_1.setBounds(237, 209, 89, 23);
		contentPane.add(button_1);
	}

}
