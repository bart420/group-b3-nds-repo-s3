import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Deliveryform extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private DeliveryPerson deliveryperson;
	private JFrame myFrame;
	/**
	 * Launch the application.
	 */
	public static void NewScreen6(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Deliveryform frame = new Deliveryform(dbobj);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Deliveryform frame = new Deliveryform();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Deliveryform(DatabaseConnector dbobj) {
		super("Delivery Form");
		myFrame = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 458, 434);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		deliveryperson = new DeliveryPerson(dbobj);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setBounds(0, 0, 514, 409);
		contentPane.add(panel);
		
		JLabel lblDeliveryPerson = new JLabel("Delivery Person");
		lblDeliveryPerson.setFont(new Font("宋体", Font.PLAIN, 16));
		lblDeliveryPerson.setBounds(164, 2, 244, 49);
		panel.add(lblDeliveryPerson);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(283, 61, 125, 108);
		panel.add(lblNewLabel);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("宋体", Font.PLAIN, 13));
		lblName.setBounds(37, 64, 81, 15);
		panel.add(lblName);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("宋体", Font.PLAIN, 13));
		lblUsername.setBounds(37, 89, 81, 15);
		panel.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("宋体", Font.PLAIN, 13));
		lblPassword.setBounds(37, 120, 81, 15);
		panel.add(lblPassword);
		
		JLabel label_4 = new JLabel("Address:");
		label_4.setFont(new Font("宋体", Font.PLAIN, 13));
		label_4.setBounds(37, 183, 81, 15);
		panel.add(label_4);
		
		JLabel lblAssignedRegion = new JLabel("Assigned Region:");
		lblAssignedRegion.setFont(new Font("宋体", Font.PLAIN, 13));
		lblAssignedRegion.setBounds(37, 256, 112, 15);
		panel.add(lblAssignedRegion);

		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(164, 61, 96, 21);
		panel.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(164, 86, 96, 21);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(164, 117, 96, 21);
		panel.add(textField_2);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(164, 179, 115, 57);
		panel.add(textArea_1);
		
		JLabel label_7 = new JLabel("Phone Number:");
		label_7.setFont(new Font("宋体", Font.PLAIN, 13));
		label_7.setBounds(37, 145, 96, 15);
		panel.add(label_7);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(164, 148, 96, 21);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(164, 253, 96, 21);
		panel.add(textField_4);
		
		JButton button = new JButton("Create");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				String r =deliveryperson.createDeliveryPerson(
					textField.getText(),
					textField_1.getText(),
					textField_2.getText(),
					textField_3.getText(),
					textArea_1.getText(),
					textField_4.getText()
				);
				JOptionPane.showMessageDialog(null,"Delivery Person created successfully !");
				lblNewLabel.setText(r);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			}
		});
		button.setFont(new Font("宋体", Font.PLAIN, 13));
		button.setBounds(37, 355, 115, 32);
		panel.add(button);
		
		JButton button_1 = new JButton("Cancel");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					DeliveryChoose nw=new DeliveryChoose(dbobj);
					nw.NewScreen5(dbobj);
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_1.setFont(new Font("宋体", Font.PLAIN, 13));
		button_1.setBounds(229, 355, 115, 32);
		panel.add(button_1);
		
		
	}
}
