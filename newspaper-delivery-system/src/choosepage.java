import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class choosepage extends JFrame {

	private JPanel contentPane;
	private DatabaseConnector dbc;
	private JFrame myFrame;
	/**
	 * Launch the application.
	 */

	public static void NewScreen3(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					choosepage frame = new choosepage(dbobj);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param dbobj 
	 */
	public choosepage(DatabaseConnector dbobj) throws Exception {
		super("Choose Page");
		myFrame = this;
		dbc = new DatabaseConnector();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 466, 392);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Customer Options");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			try {
				CustChoose nw=new CustChoose(dbc);
				nw.NewScreen4(dbc);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			try {
				myFrame.setVisible(false);
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			
			}
		});
		btnNewButton.setFont(new Font("����", Font.PLAIN, 16));
		btnNewButton.setBounds(133, 30, 164, 62);
		contentPane.add(btnNewButton);
		
		JButton btnDeliveryPerson = new JButton("Delivery Person");
		btnDeliveryPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			try {
				DeliveryChoose nw=new DeliveryChoose(dbc);
				nw.NewScreen5(dbc);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			try {
				myFrame.setVisible(false);
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			}
		});
		btnDeliveryPerson.setFont(new Font("����", Font.PLAIN, 15));
		btnDeliveryPerson.setBounds(133, 102, 164, 62);
		contentPane.add(btnDeliveryPerson);
		
		JButton btnNewButton_1 = new JButton("Cancel");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			try {
				NewsagentMenu nw=new NewsagentMenu(dbobj);
				nw.NewsagentMenuScreen(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			try {
				myFrame.setVisible(false);
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			}
		});
		btnNewButton_1.setBounds(164, 309, 103, 34);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Publication");
		btnNewButton_2.setFont(new Font("Dialog", Font.PLAIN, 15));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			try {
				Pubform nw=new Pubform(dbc);
				nw.NewScreen11(dbc);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			try {
				myFrame.setVisible(false);
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			}
		});
		btnNewButton_2.setBounds(133, 188, 164, 53);
		contentPane.add(btnNewButton_2);
	}
}
