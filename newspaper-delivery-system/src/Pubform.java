import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Pubform extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private Publication pub;
	String freq;
	private JFrame myFrame;
	
	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pubform frame = new Pubform();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/
	
	public static void NewScreen11(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pubform frame = new Pubform(dbobj);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Pubform(DatabaseConnector dbobj) {
		super("Publication Form");
		myFrame = this;
		pub = new Publication(dbobj);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 514, 348);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPublication = new JLabel("Publication Form");
		lblPublication.setFont(new Font("Dialog", Font.PLAIN, 16));
		lblPublication.setBounds(117, 11, 154, 49);
		contentPane.add(lblPublication);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(28, 69, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblPublicationDate = new JLabel("Publication Date: ");
		lblPublicationDate.setBounds(28, 94, 116, 14);
		contentPane.add(lblPublicationDate);
		
		JLabel lblCost = new JLabel("Cost: ");
		lblCost.setBounds(28, 119, 46, 14);
		contentPane.add(lblCost);
		
		JLabel lblPublicationFrequency = new JLabel("Publication Frequency: ");
		lblPublicationFrequency.setBounds(28, 145, 138, 14);
		contentPane.add(lblPublicationFrequency);
		
		JLabel lblGenre = new JLabel("Genre:");
		lblGenre.setBounds(28, 170, 46, 14);
		contentPane.add(lblGenre);

		textField = new JTextField();
		textField.setBounds(171, 66, 124, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(314, 29, 146, 104);
		contentPane.add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setBounds(171, 91, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(171, 116, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Daily");
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				freq = "daily";
			}
		});
		rdbtnNewRadioButton.setBounds(172, 141, 71, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Weekly");
		rdbtnNewRadioButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				freq = "weekly";
			}
		});
		rdbtnNewRadioButton_1.setBounds(245, 141, 74, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Monthly");
		rdbtnNewRadioButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				freq = "monthly";
			}
		});
		rdbtnNewRadioButton_2.setBounds(321, 141, 71, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		textField_3 = new JTextField();
		textField_3.setBounds(172, 167, 85, 21);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				String r= pub.createPublication(
					textField.getText(),
					textField_1.getText(),
					Double.parseDouble(textField_2.getText()),
					freq,
					textField_3.getText()
				);
				JOptionPane.showMessageDialog(null,r);
				//lblNewLabel.setText(r);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			}
		});
		btnNewButton.setBounds(100, 209, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			try {
				choosepage nw=new choosepage(dbobj);
				nw.NewScreen3(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			
			try {
				myFrame.setVisible(false);
			} catch (Exception ex1) {
				ex1.printStackTrace();
			}
			}
		});
		btnNewButton_1.setBounds(236, 209, 89, 23);
		contentPane.add(btnNewButton_1);
		
		
	}
}
