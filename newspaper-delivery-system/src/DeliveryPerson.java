import java.sql.ResultSet;
import java.util.ArrayList;

public class DeliveryPerson {

	String name;
	String username;
	String password;
	String phoneNumber;
	String address;
	String region;

	String[] validUsername = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s",
			"t", "u", "y", "w", "v", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
			"P", "R", "S", "T", "U", "Y", "W", "V", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "_" };

	String[] validNumber = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
	String[] invalidChars = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "!", "'", "�", "$", "%", "^", "&", "*",
			"(", ")", "-", "_", "+", "=", "�", "`", "[", "]", "{", "}", "#", "@", "\\", "/", "|", "<", ">", ",", ".",
			"~", "\"", "?" };
	
	String[] invalidCharsAddress = { "!", "'", "�", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "�", "`",
			"[", "]", "{", "}", "#", "@", "\\", "|", "<", ">", "~", "\"", "?" };

	DatabaseConnector dbc;

	public DeliveryPerson(DatabaseConnector dbobj) {
		dbc = dbobj;
	}

	// Name Validation
	boolean validateName(String name) {
		boolean result = true;

		for (int i = 0; i < invalidChars.length; i++) {
			if (name.indexOf(invalidChars[i]) >= 0) {
				result = false;
				break;
			}
			;
		}
		return result;
	}

	// Username Validation
	boolean validateUsername(String username) {
		for (int i = 0; i < username.length(); i++) {
			for (int j = 0; j < validUsername.length; j++) {
				if (username.substring(i, i + 1).equals(validUsername[j])) {
					break;
				} else if ((j + 1) == validUsername.length) {
					return false;
				}
			}
		}
		return true;
	}

	// Phone Number Validation
	boolean validatePhoneNumber(String phoneNumber) {
		for (int i = 0; i < phoneNumber.length(); i++) {
			for (int j = 0; j < validNumber.length; j++) {
				if (phoneNumber.substring(i, i + 1).equals(validNumber[j])) {
					break;
				} else if ((j + 1) == validNumber.length) {
					return false;
				}
			}
		}
		
		return true;
	}

	// Password Validation
	boolean validatePassword(String password) {
		boolean result = true;
		// validatePassword returns true if the password is between
		// 4 and 9 characters. Otherwise it returns false.

		if (password.length() >= 1 && password.length() <= 3)
			result = false;

		else if (password.length() >= 4 && password.length() <= 9)
			result = true;

		else if (password.length() >= 10 && password.length() <= 100)
			result = false;

		return result;
	}
	
	boolean validateAddress(String address) {
		boolean result = address.equals("") ? false : true;

		for (int i = 0; i < invalidCharsAddress.length; i++) {
			if (address.indexOf(invalidCharsAddress[i]) >= 0) {
				result = false;
				break;
			}
		}

		return result;

	}

	// Region ID Validation
	boolean validateAssignedRegion(String region) {
		if (region != "") {
			return true;
		} else {
			return false;
		}
	}

	public String createDeliveryPerson(String name, String username, String password, String phoneNumber, String address,String region) throws Exception {
		String result = "ok";
		boolean vr1 = validateName(name);
		boolean vr2 = validateUsername(username);
		boolean vr3 = validatePassword(password);
		boolean vr4 = validatePhoneNumber(phoneNumber);
		boolean vr5 = validateAddress(address);
		boolean vr6 = validateAssignedRegion(region);

		if (vr1 == false) {
			result = "Incorrect Name";
		} else if (vr2 == false) {
			result = "Incorrect Username";
		} else if (vr3 == false) {
			result = "Incorrect Password";
		} else if (vr4 == false) {
			result = "Incorrect Phone Number";
		}else if (vr5 == false) {
			result = "Incorrect Address";
		} else if (vr6 == false) {
			result = "Incorrect Region(s)";
		} else if ((vr1 == true) && (vr2 == true) && (vr3 == true) && (vr4 == true) && (vr5 == true) && (vr6 == true)) {
			result = dbc.createDeliveryPersonInDatabase(name, username, password, phoneNumber, address, region) ? "ok" : "fail";
		}
		return result;
	}
	
	public ResultSet viewDeliveryPersonDetails(String deliveryPerson) throws Exception {
		if (!deliveryPerson.trim().equals("")) {
			return dbc.retrieveDeliveryDriverDetails(deliveryPerson);
		} else
			return null
		;
	}
}