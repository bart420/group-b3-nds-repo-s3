import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeliveryChoose extends JFrame {

	private JPanel contentPane;
	private JFrame myFrame;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeliveryChoose frame = new DeliveryChoose();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/
	public static void NewScreen5(DatabaseConnector dbobj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeliveryChoose frame = new DeliveryChoose(dbobj);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DeliveryChoose(DatabaseConnector dbobj) {
		super("Delivery Choose");
		myFrame = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 460, 422);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAddNewDeliery = new JButton("Add New Delivery Person");
		btnAddNewDeliery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				Deliveryform nw=new Deliveryform(dbobj);
				nw.NewScreen6(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		btnAddNewDeliery.setFont(new Font("����", Font.PLAIN, 12));
		btnAddNewDeliery.setBounds(101, 76, 191, 67);
		contentPane.add(btnAddNewDeliery);
		
		JButton btnSearchDeliveryPerson = new JButton("Search Delivery Person");
		btnSearchDeliveryPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ViewDeliveryPerson nw=new ViewDeliveryPerson(dbobj);
				nw.ViewDeliveryPersonScreen(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		btnSearchDeliveryPerson.setFont(new Font("����", Font.PLAIN, 14));
		btnSearchDeliveryPerson.setBounds(101, 190, 191, 67);
		contentPane.add(btnSearchDeliveryPerson);
		
		JButton button_2 = new JButton("BACK");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				choosepage nw=new choosepage(dbobj);
				nw.NewScreen3(dbobj);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
				try {
					myFrame.setVisible(false);
				} catch (Exception ex1) {
					ex1.printStackTrace();
				}
			}
		});
		button_2.setFont(new Font("����", Font.PLAIN, 16));
		button_2.setBounds(149, 314, 106, 36);
		contentPane.add(button_2);
	}

}
